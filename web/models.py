from django.db import models
from django.utils.translation import ugettext_lazy as _
from stdimage import StdImageField


class Pipe(models.Model):
	name = models.CharField(_('name'), max_length=255, null=False, blank=False)
	description = models.TextField(_('description'), null=True, blank=True)

class Image(models.Model):
	source = StdImageField(upload_to='pipes', blank=False, null=False, variations={'thumbnail': (100, 100, True)})
	pipe = models.ForeignKey(Pipe, null=False, blank=False)

